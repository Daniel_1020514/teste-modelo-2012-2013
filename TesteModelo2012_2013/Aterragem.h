#pragma once
#include<iostream>
#include<string>
#include"Pedidos.h"
using namespace std;

class Aterragem :public Pedidos{


private:
	double qtdFuel;

public:
	Aterragem();
	Aterragem(int, double);
	Aterragem(const Aterragem&);
	~Aterragem();
	

	Aterragem& operator=(Aterragem&);

	double getQtdFuel(void);
	void setQtdFuel(double);
	virtual Pedidos* clone();
	void escreve(ostream&);
};

Aterragem::Aterragem(){}
Aterragem::Aterragem(int c, double q) :Pedidos(c){
	qtdFuel = q;
}
Aterragem::Aterragem(const  Aterragem & at){
	qtdFuel = at.qtdFuel;
}

Aterragem::~Aterragem(){}
double Aterragem::getQtdFuel(){
	return qtdFuel;
}
Pedidos* Aterragem::clone(){
	return new Aterragem(*this);
}

void Aterragem::escreve(ostream & out){
	Pedidos::escreve(out);
	cout << "Quantidade de combustivel: " << qtdFuel << endl;

}
ostream& operator<<(ostream& out, Aterragem &a){
	a.escreve(out);
	return out;
}