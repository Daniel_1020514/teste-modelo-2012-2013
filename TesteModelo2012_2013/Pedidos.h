#pragma once
#include<string>
#include<iostream>

using namespace std;

class Pedidos{

private: 
	int codAviao;


public:

	Pedidos();
	Pedidos(int);
	~Pedidos();
	Pedidos(const Pedidos&);

	


	int getCodAviao(void);
	void setCodAviao(int);

	virtual void escreve(ostream&);
	virtual  Pedidos* clone();
	Pedidos& operator=(Pedidos&);

};
Pedidos::Pedidos(){}
Pedidos::Pedidos(int c){
	codAviao = c;

}
Pedidos::Pedidos(const Pedidos& p){
	codAviao = p.codAviao;
}

Pedidos::~Pedidos(){}
Pedidos& Pedidos::operator=(Pedidos&p){
	if (this != &p){
		codAviao = p.codAviao;

	}
	return (*this);
}
void Pedidos::escreve(ostream& out){
	cout<<"Codigo do aviao: "<<codAviao<<endl;
}
Pedidos* Pedidos::clone(){

	return new Pedidos(*this);
}
ostream& operator<<(ostream& out, Pedidos &p){
	p.escreve(out);
	return out;
}