#pragma once
#include<iostream>
#include<string>
#include"Pedidos.h"
using namespace std;

class Descolagem :public Pedidos{


private:
	string destino;

public:
	Descolagem();
	Descolagem(int, string);
	Descolagem(const Descolagem&);
	~Descolagem();
	virtual Pedidos* clone();

	Descolagem& operator=(Descolagem&);

	string getDestino(void);
	void setDestino(string);

	void escreve(ostream&);
};
Descolagem::~Descolagem(){}
Descolagem::Descolagem(){
	destino = "";
}
Descolagem::Descolagem(int cd, string d):Pedidos(cd){
	destino = d;
}
Descolagem::Descolagem(const Descolagem &d){
	destino = d.destino;

}
string Descolagem::getDestino(){
	return destino;
}

void Descolagem::escreve(ostream & out){
	Pedidos::escreve(out);
	cout << "Destino: " << destino<< endl;

}
Pedidos* Descolagem::clone(){
	return new Descolagem(*this);
}
ostream& operator<<(ostream& out, Descolagem &d){
	d.escreve(out);
	return out;
}