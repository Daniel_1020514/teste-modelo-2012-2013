#pragma once
#include<typeinfo>
#include<iostream>
#include<string>
#include<list>
#include"Aterragem.h"
#include"Descolagem.h"
#include"Pedidos.h"
using namespace std;

class Aeroporto{
private:
	list<Pedidos*> p;
	double totLitros;
public:

	Aeroporto();
	//	~Aeroporto();
	void criarPedidos();
	//void escreve(ostream&);
	void calculaLitros();
	void listarPedidos();
	void inserePedido(Pedidos*);
};
Aeroporto::Aeroporto(){
	totLitros = 0;
}


void Aeroporto::listarPedidos(){
	list<Pedidos*>::iterator it1;

	for (it1 = p.begin(); it1 != p.end(); it1++){

		cout << (**it1) << endl;
	}
}



//void Aeroporto::escreve(ostream&out){
//	list<Pedidos*>::iterator it1;
//
//	for (it1 = p.begin(); it1 != p.end(); it1++){
//
//		cout << (**it1) << endl;
//	}
//}
void Aeroporto::criarPedidos(){

	Aterragem *at1 = new Aterragem(1, 200);
	Aterragem *at2 = new Aterragem(2, 300);
	Aterragem *at3 = new Aterragem(3, 400);
	Aterragem *at4 = new Aterragem(4, 500);

	Descolagem *d1 = new Descolagem(5, "Porto");
	Descolagem *d2 = new Descolagem(6, "Lisboa");
	Descolagem *d3 = new Descolagem(7, "Faro");


	p.push_back(at1);
	p.push_back(d2);
	p.push_back(d1);
	p.push_back(at3);
	p.push_back(at4);
	p.push_back(at2);


	p.push_back(d3);

}
/*b) Implemente o m�todo na classe aeroporto que calcula o valor total em litros dos v�rios avi�es que 
efetuaram um pedido de aterragem. 
*/
void Aeroporto::calculaLitros(){

	list<Pedidos*>::iterator it1;
	for (it1 = p.begin(); it1 != p.end(); it1++){
		if (typeid(**it1) == typeid(Aterragem)){
			Aterragem* at = dynamic_cast<Aterragem*>(*it1);
			totLitros += at->getQtdFuel();
		}
	}
	cout << "Total de litros: " << totLitros << endl;
}



/*2.Usando os m�todos da classe Lista elabore o m�todo que adiciona um novo pedido � lista de pedidos 
do aeroporto, sabendo que os pedidos de aterragem t�m prioridade sobre todos os pedidos de 
descolagem e s�o inseridos por ordem de chegada.*/
void Aeroporto::inserePedido(Pedidos *pd){

	list<Pedidos*>::iterator it;
	for (it = p.begin(); it != p.end(); it++){
		if (typeid(**it) == typeid(Descolagem)){
			p.insert(it, pd);
			break;
		}
	}

}

//ostream& operator<<(ostream& out, Aeroporto &a){
//	a.escreve(out);
//	return out;
//}